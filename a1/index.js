/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console. 
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function askUserInfo(){
		let fullName, age , location;

		fullName = prompt ('What is your full name?')
		age = prompt ('How old are you?')
		location = prompt ('What is your location?')

		alert("Thank you for your input!")

		console.log("Your full name is: " + fullName);
		console.log("Your age is: " + age);
		console.log("Your location is: " + location);


	}
	askUserInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topFiveBand(){

		let band = ["EXO","December Avenue", "Ben and bens", "This Band", "Itchy Worms"]

		for (let i = 0; i < band.length; i++) {
		console.log("" +(i+1)+" " +band[i]);
		}


	}

	topFiveBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topFiveMovies(){
			console.log('1. GLASS ONION: A KNIVES OUT MYSTERY\n\nRotten Tomatoes Rating : 93%');
			console.log('2. THE INSPECTION\n\nRotten Tomatoes Rating : 84%');
			console.log('3. VIOLENT NIGHT\n\nRotten Tomatoes Rating : 71%');
			console.log('4. LAST FILM SHOW\n\nRotten Tomatoes Rating : 94%');
			console.log("5.ALMA'S RAINBOW \n\nRotten Tomatoes Rating : 83%");
		};

		topFiveMovies(); 

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printFriends() = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
// };


// console.log(printUsers);
// console.log(printFriends);