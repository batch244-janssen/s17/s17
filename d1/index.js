// [ SECTION ] Functions 
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession

		// console.log("Bullet is my name.");
		
	// Function declarations
		// (function statment) defines a function with the specified parameters.

			// Syntax: 
				/*
					function functionName(){
						code block (statement);
					};
		
				*/

			// function keyword - used to defined a javascript functions
			// functionName - the function name. Functions are named to be able to use later in the code.
			// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.
		
	function functionName(){
		console.log('This is bullet.');
	};

	functionName(); // Results in an error, much like variables, we cannot invoke a function we have yet to define. 

// [ SECTION ] Function Invocation 

		// functionName()

	// The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	// It is common to use the term "call a function" instead of "invoke a function".

		//Let's invoke/call the functions that we declared.
		functionName();

		let isLegalAge = true;
		let isRegistered = false;


		// Another Example of Functions
		function logicalOperator(){
			let allRequirementsMet = isLegalAge && isRegistered;
			console.log(allRequirementsMet);
			// console.log('1. Jollibee');
		};

		logicalOperator();

		// Mini-Activity:
		// Create a function which is able to print your Top 3 favorite fast-food store

			function favoriteStore(){
				console.log('1. Jollibee\n\n2. KFC');
				console.log('2. KFC');
				console.log('3. Mcdo');
			};

			favoriteStore(); 

			functionName();

// [ SECTION ] Function declarations vs expressions
	
	// Function Declaration

		// A function can be created through function declaration by using the function keyword and adding a function name.
		//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
		//declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

		//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

			function declaredFunction() {
				console.log("Hello World from declaredFunction()")
			}

			declaredFunction();

	// Function Expression

		// A funcation can also be stored in a variable. This is called a function expression. 
		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

			// variableFunction();
			/* 
				error - function expressions, being stored in a let or const variable, cannot be hoisted.
			*/

		let variableFunction = function() {
			console.log('Hellow Again!');
		};

		variableFunction();

			//A function expression of a function named func_name assigned to the variable funcExpression
			//How do we invoke the function expression?
			//They are always invoked (called) using the variable name.

			let funcExpression = function funcName() {
				console.log("Hellow from the other side!")
			};

			funcExpression();

			// Mini-Activity
				// Since funcExpression is declared as let, re-assign the following statement into console.log('Hellow');

			//You can reassign declared functions and function expressions to new anonymous functions.

			funcExpression = function funcName(){
				console.log('Hellow');
			};

			funcExpression();

			//However, we cannot re-assign a function expression initialized with const.

			const newExpression = function (){
				console.log('This is newExpression');
			};

			newExpression();

				// newExpression = function (){
				// 	console.log('This is the updated one!');
				// };

				// newExpression();

// [ SECTION ] Function Scoping

	/*
		Scope is the accessibility (visibility/availability) of variables, 

		{} - Block

		Javascript Variables has 3 type of scope:
			1. local/block scope
			2. global scope
			3. function scope
					JavaScript has function scope: Each function creates a new scope.
					Variables defined inside a function are not accessible (visible) from outside the function.
					Variables declared with var, let and const are quite similar when declared inside a function
			
	*/
			{
				let localVar = "Michael Jordan";
				console.log(localVar); // Result would be, Michael Jordan
			}

			let globalVar = 'Mr. Worldwide';
			// console.log(localVar); Result would be, Error

		function showNames(){
			let myName = "Carlo";
			const myFavoriteEmelHero = 'Pharsa';

			console.log(myName);
			console.log(myFavoriteEmelHero);  
		}

		showNames();

			// console.log(myName); // Result would be, Error
			// console.log(myFavoriteEmelHero); // Result would be, Error

		// Nested Functions 

			//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

				function myNewFunction(){
					let name = "Mike";

					function nestedFunction(){
						let myFavoriteEmelHero = "Uranus";
						console.log(myFavoriteEmelHero);
						console.log(name)
					}

					nestedFunction();
					// console.log(myFavoriteEmelHero); // Result would be, error
					//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.

				}

				myNewFunction();
				//nestedFunction();//results to an error. 

				// Moreover, since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in.

		// Function and Global Scoped Variables

			// Global Scoped Variable
				let globalName = 'VeeWise';

				function blacklistEmel(){
					let reserved = 'Kevier';

					//Variables declared Globally (outside any function) have Global scope.
					//Global variables can be accessed from anywhere in a Javascript 
					//program including from inside a function.

					console.log(globalName);
					console.log(reserved);
				}

				blacklistEmel();
				// console.log(reserved); // Result would be, Error
					// nameInside is function scoped. It cannot be accessed globally.

// [ SECTION ] Using Alert()
	
	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

		alert('Hellow, Carlo!\n\nFrom Line 243'); // This will immediately run when the page loads.

		function showSampleAlert(){
			alert('Hellow Again, C4rlo!\n\nFrom Line 246');
		};

		showSampleAlert();

			//You will find that the page waits for the user to dismiss the dialog before proceeding. You can witness this by reloading the page while the console is open.

		console.log("I will only log in the console when the alert is dismissed.\n\nFrom Line 253");

			//Notes on the use of alert():
			//Show only an alert() for short dialogs/messages to the user. 
			//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [ SECTION ] Using Prompt()
	
	//prompt() allows us to show a small window at the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

		let samplePrompt = prompt("Enter your Name.");
		//console.log(typeof samplePrompt);//The value of the user input from a prompt is returned as a string.

		// let alerta = alert('Hellow, ' + samplePrompt);

			/*

				Syntax:
					prompt('<dialogInStrings');

			*/

		let sampleNullPrompt = prompt("Don't enter anything.");
		console.log(sampleNullPrompt); // prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

		// prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

		// prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

		// Mini-Activity 
			// Let's create a function scoped variables to store the returned data from our prompt(). This way, we can dictate when to use a prompt() window or have a reusable function to use our prompts.

			function printWelcomeMessage(){
				let firstName = prompt("Enter your First Name");
				let lastName = prompt("Enter your Last Name");

				console.log('Hello, ' + firstName + " " + lastName + "!");
				console.log('Welcome to my page!');
			};

			printWelcomeMessage();

// [ SECTION ] Function Naming Conventions

	// Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){

			let courses = ['Science 101', 'Math 101', "English 101"];
			console.log(courses); 

			let mplTeams = ["Blacklist", 'Echo', 'Bren'];
			console.log(mplTeams)
		};

		getCourses();

	// Avoid generic names to avoid confusion within your code.

		function getFavoritesGames(){
			let games = ['Stumble', 'City Skylines', 'Mobile Legends'];
			console.log(games);
		};

		getFavoritesGames();

	// Avoid pointless and inappropriate function names.

		function foo(){
			console.log("I Love Javascript!");
		};

		foo();

	//Name your functions in small caps. Follow camelCase when naming variables and functions.

		function displayCarInfo(){

			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		
		displayCarInfo();

